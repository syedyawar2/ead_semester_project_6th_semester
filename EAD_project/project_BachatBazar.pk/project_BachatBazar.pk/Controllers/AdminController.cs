﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using project_BachatBazar.Models;
using System.Net;
using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;


namespace project_BachatBazar.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/



        public ActionResult home()
        {
            return View();
        }



        [HttpPost]
        public JsonResult SaveSpecs(inputDTO ob)
        {
            int cid = 0;
            specsDTO dto =new specsDTO();

          //  var URL = "http://www.compareraja.in/mobiles/motorola-mobile-prices.html";
            var URL = ob.link;
            var _wReq = (HttpWebRequest)WebRequest.Create(URL);
            _wReq.Method = "GET";
            String reponseHtml = "";

            HttpWebResponse _wResp = (HttpWebResponse)_wReq.GetResponse();
            using (System.IO.StreamReader _sr = new System.IO.StreamReader(_wResp.GetResponseStream()))
            {
                reponseHtml = _sr.ReadToEnd();
            }

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(reponseHtml);

            var divs = doc.DocumentNode.QuerySelectorAll("div.prodcut-detail").ToList();

            for (int j = 0; j < 20; j++)
            {
                var jobdiv = divs[j];
                var doc1 = new HtmlAgilityPack.HtmlDocument();
                doc1.LoadHtml(jobdiv.InnerHtml);
                String jtitle = doc1.DocumentNode.QuerySelector("a").Attributes["href"].Value;
                String mName = doc1.DocumentNode.QuerySelector("a").Attributes["title"].Value;
                dto.model = mName;
               // dto.company = "Motorolla";
                dto.company = ob.company;

                var URL1 = jtitle;
                var _wReq1 = (HttpWebRequest)WebRequest.Create(URL1);
                _wReq1.Method = "GET";
                String reponseHtml1 = "";

                HttpWebResponse _wResp1 = (HttpWebResponse)_wReq1.GetResponse();
                using (System.IO.StreamReader _sr = new System.IO.StreamReader(_wResp1.GetResponseStream()))
                {
                    reponseHtml1 = _sr.ReadToEnd();
                }

                HtmlAgilityPack.HtmlDocument doc2 = new HtmlAgilityPack.HtmlDocument();
                doc2.LoadHtml(reponseHtml1);






                var trs1 = doc2.DocumentNode.QuerySelectorAll("a.simpleLens-thumbnail-wrapper").ToList();
                for (int i = 0; i < trs1.Count; i++)
                {
                    var trRow = trs1[i];
                    var doc3 = new HtmlAgilityPack.HtmlDocument();
                    doc3.LoadHtml(trRow.InnerHtml);
                    //String title1 = doc3.DocumentNode.QuerySelector("th.hdngArial").InnerText;
                    String title2 = doc3.DocumentNode.QuerySelector("img").Attributes["src"].Value;
                    title2 = title2.Substring(title2.LastIndexOf("/") + 1);
                    if (i == 0) { dto.picture1 = title2; }
                    else if (i == 1) { dto.picture2 = title2; }
                    else if (i == 2) { dto.picture3 = title2; }
                    // MessageBox.Show(title2);



                }





                dto.price = new List<PriceDTO>();
                PriceDTO p = new PriceDTO();

                var boxes = doc2.DocumentNode.QuerySelectorAll("div.mer-box1").ToList();
                for (int i = 0; i < boxes.Count; i++)
                {


                    var trRow = boxes[i];
                    var doc3 = new HtmlAgilityPack.HtmlDocument();
                    doc3.LoadHtml(trRow.InnerHtml);
                    //String title1 = doc3.DocumentNode.QuerySelector("th.hdngArial").InnerText;
                    if (doc3.DocumentNode.QuerySelector("img") != null)
                    {
                        String img = doc3.DocumentNode.QuerySelector("img").Attributes["src"].Value;
                        if (img.Contains("ebay"))
                        {
                            String title2 = doc3.DocumentNode.QuerySelector("a").Attributes["onclick"].Value;
                            string[] tokens = title2.Split('&');
                            string[] tokens1 = tokens[1].Split('=');
                            //  MessageBox.Show(tokens1[1]);

                            p.ebay_link = tokens1[1];
                            var title3 = doc3.DocumentNode.QuerySelectorAll("p.mer-text").ToList();
                            p.ebay_price = title3[0].InnerText.ToString();
                            // dto.price.Add(p);
                        }
                        //for flipkart
                        else if (img.Contains("cb2852"))
                        {
                            String title2 = doc3.DocumentNode.QuerySelector("a").Attributes["onclick"].Value;
                            string[] tokens = title2.Split('(');
                            string[] tokens1 = tokens[1].Split('?');
                            string abc = tokens1[0].Substring(1);
                            // PriceDTO p = new PriceDTO();
                            p.flipkart_link = abc;
                            var title3 = doc3.DocumentNode.QuerySelectorAll("p.mer-text").ToList();
                            p.flipkart_price = title3[0].InnerText.ToString();
                            // dto.price.Add(p);
                            // MessageBox.Show(abc);
                        }
                        else if (img.Contains("amazon"))
                        {
                            String title2 = doc3.DocumentNode.QuerySelector("a").Attributes["onclick"].Value;
                            string[] tokens = title2.Split('(');
                            string[] tokens1 = tokens[1].Split('?');
                            string abc = tokens1[0].Substring(1);

                            //   PriceDTO p = new PriceDTO();
                            p.amazon_link = abc;
                            var title3 = doc3.DocumentNode.QuerySelectorAll("p.mer-text").ToList();
                            p.amazon_price = title3[0].InnerText.ToString();
                            //dto.price.Add(p);
                            //  MessageBox.Show(abc);
                        }
                    }
                }

                dto.price.Add(p);




                var specs = doc2.DocumentNode.QuerySelectorAll("ul.nexmob-lst-nw li").ToList();
                int j1 = 0;
                for (int i = 0; i < 9; i++)
                {
                    var trRow = specs[j1];
                    String title2 = trRow.InnerText;
                    string[] tokens = title2.Split(':');


                    if (i == 0)
                    {

                        if (tokens[1].Contains("Yes"))
                        {
                            dto.nano_sim = tokens[1];
                            j1++;

                        }
                        else
                        {
                            dto.nano_sim = "No";

                        }
                    }


                    else if (i == 1) { dto.display_size = tokens[1]; j1++; }
                    else if (i == 2) { dto.memory = tokens[1]; j1++; }
                    else if (i == 3) { dto.ram = tokens[1]; j1++; }
                    else if (i == 4) { dto.camera_rear = tokens[1]; j1++; }
                    else if (i == 5) { dto.camera_front = tokens[1]; j1++; }
                    else if (i == 6) { dto.os = tokens[1]; j1++; }
                    else if (i == 7) { dto.os_vesion = tokens[1]; j1++; }
                    else if (i == 8) { dto.processor = tokens[1]; j1++; }
                    // MessageBox.Show(tokens[0]);
                    // MessageBox.Show(tokens[1]);




                }


                bal repo = new bal();
                cid = repo.SaveCustomer(dto);



            }









            Object obj = new
            {
                Mobile_id = cid,
                success = true,
                message = "Successfully Saved"
            };

            return Json(obj);

        }

    }
}
