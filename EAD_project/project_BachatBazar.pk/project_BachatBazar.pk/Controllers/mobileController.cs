﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using project_BachatBazar.Models;

namespace project_BachatBazar.Controllers
{
    public class mobileController : Controller
    {
        //
        // GET: /mobile/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult mobiles()
        {
            return View();
        }
        public ActionResult company(int id)
        {
            bal obj = new bal();
            var data=obj.GetByCompany(id);
            if (data != null)
            {
                return View(data);
            }
            else 
            {
                return View("error");
            }
        }

        public ActionResult price(int id ,string name)
        {
            bal obj = new bal();
            var data = obj.price_specs(id);
            ViewData["obj"] = data;
            ViewData["obj1"] = data.price.FirstOrDefault();
            return View(data); 
          
        }
        public ActionResult search(string abc)
        {
            bal obj = new bal();
            var data = obj.search(abc);
            if (data != null)
            {
                ViewData["obj"] = data;
                ViewData["obj1"] = data.price.FirstOrDefault();
                return View("price", data);
            }
            else
            {
                /*
                ViewBag.message = "error - search not found !!";
                specsDTO obj2 = new specsDTO();
                PriceDTO abcde = new PriceDTO();
                ViewData["obj"] = obj2;
                ViewData["obj1"] = abcde;
                 
                return View("price",obj2);
                 */

                return View("error");
            }

        }
        public JsonResult js()
        {
            bal obj1 = new bal();
            var data = obj1.GetAll();
            
            return Json(data, JsonRequestBehavior.AllowGet);

        
        }

    }
}
