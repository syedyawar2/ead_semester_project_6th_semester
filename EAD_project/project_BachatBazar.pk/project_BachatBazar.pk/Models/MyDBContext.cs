﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;

namespace project_BachatBazar.Models
{
    public class MyDBContext : DbContext
    {
        public DbSet<specsDTO> Specs
        {
            get;
            set;
        }

        public DbSet<PriceDTO> Price
        {
            get;
            set;
        }
       

        public MyDBContext()
        {
            this.Configuration.ProxyCreationEnabled = false;
        }
    }
}