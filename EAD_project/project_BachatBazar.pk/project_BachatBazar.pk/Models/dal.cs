﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace project_BachatBazar.Models
{
    public class dal
    {
        public int SaveCustomer(specsDTO dto)
        {
            using (var dbCtx = new MyDBContext())
            {
                dbCtx.Specs.Add(dto); //Add object to save

                dbCtx.SaveChanges(); //Save in database

                return dto.mobile_id; //return the auto generated customer id
            }
        }
        public List<specsDTO> GetByCompany(string comp)
        {
            try
            {
                using (var dbCtx = new MyDBContext())
                {
                    var list=dbCtx.Specs.Where(p =>p.company==comp);
                    return list.ToList();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public Array GetAll()
        {
            try
            {
                using (var dbCtx = new MyDBContext())
                {
                    var list = dbCtx.Specs.Select(i=>i.model).ToArray();
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public specsDTO price_specs(int id)
        {
            try
            {
                using (var dbCtx = new MyDBContext())
                {
                    dbCtx.Configuration.LazyLoadingEnabled = false;
                    var list = dbCtx.Specs.Where(p => p.mobile_id == id).FirstOrDefault();
                    dbCtx.Entry(list).Collection(p => p.price).Load();
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public specsDTO search(string name)
        {
            try
            {
                using (var dbCtx = new MyDBContext())
                {
                    dbCtx.Configuration.LazyLoadingEnabled = false;
                    var list = dbCtx.Specs.Where(p => p.model == name).FirstOrDefault();
                    dbCtx.Entry(list).Collection(p => p.price).Load();
                    return list;
                }
            }
            catch (Exception ex)
            {
                
                return null;
            }
        }
    }
}