﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace project_BachatBazar.Models
{
    [Table("mobile_specs")]
    public class specsDTO
    {

        [Key]
        public int mobile_id { get; set; }
        public String model { get; set; }
        public String company { get; set; }
        public String picture1 { get; set; }
        public String picture2 { get; set; }
        public String picture3 { get; set; }
        public String display_size { get; set; }
        public String nano_sim { get; set; }
        public String memory { get; set; }
        public String os { get; set; }
        public String camera_rear { get; set; }
        public String camera_front { get; set; }
        public String ram { get; set; }
        public String os_vesion { get; set; }
        public String processor { get; set; }


        public virtual ICollection<PriceDTO> price { get; set; }
    }


    [Table("mobile_prices")]
    public class PriceDTO
    {
        [Key]
        public int price_id { get; set; }
        public int mobile_id { get; set; }
        public String amazon_price { get; set; }
        public String amazon_link { get; set; }
        public String flipkart_price { get; set; }
        public String flipkart_link { get; set; }

        public String ebay_price { get; set; }
        public String ebay_link { get; set; }

        public virtual specsDTO specs { get; set; }


    }
    public class inputDTO
    {
        public string link { get; set; }
        public string company { get; set; }
    
    }

     

}