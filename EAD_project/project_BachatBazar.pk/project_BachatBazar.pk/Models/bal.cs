﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_BachatBazar.Models
{
    public class bal
    {

        public int SaveCustomer(specsDTO dto)
        {
            dal dataService = new dal();
            int cid = dataService.SaveCustomer(dto);
            return cid;
        }
        public specsDTO price_specs(int id)
        {
            dal dataService = new dal();
            specsDTO  result = dataService.price_specs(id);
            return result;
        
        }
        public specsDTO search(string name)
        {
            dal dataService = new dal();
            specsDTO result = dataService.search(name);
            return result;

        }
        public Array GetAll()
        {
            dal dataService = new dal();
            var data=dataService.GetAll();
            return data;
        
        }
        public List<specsDTO> GetByCompany(int id)
        {
            dal dataService = new dal();
            string comp="";
            if (id == 1)
                comp = "Htc";
            else if (id == 2)
                comp = "Samsung";
            else if (id == 3)
                comp = "Apple";
            else if (id == 4)
                comp = "Sony Xperia";
            else if (id == 5)
                comp = "Microsoft";
            else if (id == 6)
                comp = "Lenovo";
            else if (id == 7)
                comp = "Micro Max";
            else if (id == 8)
                comp = "Motorolla";
            List<specsDTO> result = dataService.GetByCompany(comp);
            if (result.Count == 0) {
                return null;
            }
            return result;
        }
    }
}